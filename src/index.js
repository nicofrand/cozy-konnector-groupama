const request = require("request");
const cheerio = require("cheerio");
const moment = require("moment");

const j = request.jar();
const {
    BaseKonnector,
    saveBills,
    log,
    errors
} = require("cozy-konnector-libs");

const logger = {
    info: msg => log("info", msg),
    error: msg => log("error", msg),
    debug: msg => log("debug", msg)
};

module.exports = new BaseKonnector(start);

const authUrl = `https://authentification.groupama.fr/cas/login?client_id=ecli_groupama`;

function fetchLoginData() {
    const loginPageTokenOptions = {
        method: "GET",
        ecdhCurve: "auto",
        jar: j,
        url: authUrl
    };

    return new Promise((resolve, reject) => {
        logger.info("Retrieving login token");
        request(loginPageTokenOptions, (err, res) => {
            let token = "";
            let keypadMapping = [];

            if (!err) {
                let $ = cheerio.load(res.body);
                token = $("#fm1 input[name='execution']").val();
                if (!token)
                    err = new Error("No login token found");
                else {
                    // Can't invent a name like that
                    let christianKeyboard = res.body.match(/clavierAChristian\s?=\s?\[(.*)\]/);
                    if (christianKeyboard && christianKeyboard[1])
                        keypadMapping = christianKeyboard[1].split(', ').map(v => String.fromCharCode(65 + parseInt(v, 10)));

                    if (!keypadMapping.length)
                        err = new Error("No login keypad mapping found");
                }
            }

            if (err) {
                logger.debug(err.message);
                logger.error("Could not retrieve login token or keypad mapping");
                return reject(new Error(errors.LOGIN_FAILED));
            }

            return resolve({
                token,
                keypadMapping
            });
        });
    });
}

function fetchXSRFCookie() {
    const xsrfCookieOptions = {
        method: "GET",
        ecdhCurve: "auto",
        jar: j,
        url: `https://authentification.groupama.fr/cms/get-article-content/site-name/Guest/article-id/MOBILE_INFO?_=${Date.now()}`
    };

    return new Promise((resolve, reject) => {
        logger.info("Retrieving XSRF cookie");
        request(xsrfCookieOptions, (err, res) => {
            return resolve();
        });
    });
}


function authenticate(loginData, requiredFields) {
    const password = requiredFields.password.split("").map(c => loginData.keypadMapping[parseInt(c, 10)]).join("");

    const signInOptions = {
        method: "POST",
        ecdhCurve: "auto",
        jar: j,
        url: authUrl,
        form: {
            username: requiredFields.username,
            password,
            execution: loginData.token
        }
    };

    return new Promise((resolve, reject) => {
        logger.info("Signing in");
        request(signInOptions, (err, res, body) => {
            let errType = "";
            if (err) {
                errType = errors.VENDOR_DOWN;
            }

            if (errType) {
                logger.error("Signin failed");
                return reject(new Error(errType));
            }

            logger.info("Signed in successfully");
            return resolve();
        });
    });
}

function fetchDocuments() {
    const documentsOptions = {
        method: "POST",
        ecdhCurve: "auto",
        jar: j,
        url: "https://espaceclient.groupama.fr/api/ecli/documents/synthese"
    };

    return new Promise((resolve, reject) => {
        logger.info("Fetching documents");
        request(documentsOptions, (err, res, body) => {
            if (!err && res.headers.location.includes("login"))
                err = new Error("Oops, session already invalid");

            if (!err && !body) {
                err = new Error("No documents list received");
            }

            if (!err) {
                try {
                    body = JSON.parse(body);
                    body = body.documents;
                } catch (err) {
                    logger.error("Cannot parse response body");
                    err = err;
                }
            }

            if (err) {
                logger.debug(err);
                return reject(err);
            }

            let docs = body;
            if (!err) {
                // TODO
            }

            return resolve(docs);
        });
    });
}

function fetchContracts() {
    const contractsOptions = {
        method: "POST",
        ecdhCurve: "auto",
        jar: j,
        url: "https://espaceclient.groupama.fr/api/ecli/navigation/synthese?onglet=NAV_ONGL_PRIV"
    };

    return new Promise((resolve, reject) => {
        logger.info("Fetching contracts");
        request(contractsOptions, (err, res, body) => {
            if (!err && res.headers.location.includes("login"))
                err = new Error("Oops, session lost");

            if (!err && !body) {
                err = new Error("No contracts body received");
            }

            if (!err) {
                try {
                    body = JSON.parse(body);
                    body = contracts.entries[0].entries;
                } catch (err) {
                    logger.error("Cannot parse response");
                    err = err;
                }
            }

            if (err) {
                logger.debug(err);
                return reject(err);
            }

            let contracts = null;
            if (!err) {
                contracts = [];
                for (let c of body) {
                    for (let item of c.contratItems) {
                        item = item.contrat;

                        contracts.push({
                            id: item.identifiant,
                            code: item.produit.code,
                            category: item.produit.categorie,
                            family: item.produit.famille,
                            label: item.produit.libelle
                        });
                    }
                }
            }

            return resolve(contracts);
        });
    });
}

async function start(fields) {
    const loginData = await fetchLoginData();
    await authenticate(loginData, fields);
    await fetchXSRFCookie();

    // Fetch documents
    const docs = await fetchDocuments();
    console.log(docs);

    // Fetch health reimbursements
    /*
    const contracts  = await fetchContracts();
    console.log(contracts);
    //https://espaceclient.groupama.fr/api/ecli/navigation/synthese?onglet=NAV_ONGL_PRIV
    */

    /*
    const loginToken = await fetchLoginToken();
    await login(loginToken, fields);
    const billsPeriods = await fetchBillsPeriods();
    const bills = await fetchBills(billsPeriods);

    logger.info(`${bills.length} bill(s) retrieved`);

    await saveBills(bills, fields.folderPath, {
        identifiers: ["materiel.net"]
    });
    */
}